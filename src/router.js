import { createRouter, createWebHashHistory } from "vue-router";
import App from "./App.vue";

import LogIn from "./components/Login.vue";
import SignUp from "./components/SignUp.vue";
import Home from "./components/Home.vue";
// import Account from './components/Account.vue'
import Categories from "./components/Categories.vue";
import CreateCategory from "./components/CreateCategory.vue";
import Ciudades from "./components/Ciudades.vue";
import CreateCiudad from "./components/CreateCiuad.vue";
import Lugares from "./components/Lugares.vue";

const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/user/logIn",
    name: "logIn",
    component: LogIn,
  },
  {
    path: "/user/signUp",
    name: "signUp",
    component: SignUp,
  },
  {
    path: "/user/home",
    name: "home",
    component: Home,
  },
  {
    path: "/user/categorias",
    name: "categorias",
    component: Categories,
  },

  {
    path: "/user/ciudades",
    name: "ciudades",
    component: Ciudades,
  },
  {
    path: "/user/lugares",
    name: "lugares",
    component: Lugares,
  },
  {
    path: "/user/createcategory/",
    name: "createcategory",
    component: CreateCategory,
  },
  {
    path: "/user/createciudad/",
    name: "createciudad",
    component: CreateCiudad,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
